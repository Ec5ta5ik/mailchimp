<?php

namespace Gentlefox\Mailchimp;

use Illuminate\Support\ServiceProvider;

class MailchimpServiceProvider extends ServiceProvider
{

	public function boot()
	{
		$this->publishes([
			__DIR__ .'/../../config.php' => config_path('mailchimp.php'),
		]);
	}

	public function register()
	{
		$this->app->singleton(Mailchimp::class, function($app) {
			return new Mailchimp(config('mailchimp.dataCenter'), config('mailchimp.username'), config('mailchimp.key'));
		});
	}

}