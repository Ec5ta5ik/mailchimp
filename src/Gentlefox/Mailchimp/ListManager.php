<?php

namespace Gentlefox\Mailchimp;

class ListManager {

	/**
	 * @var Mailchimp
	 */
	private $mailChimp;

	/**
	 * raw array of lists
	 * @var array
	 */
	private $lists;

	public function __construct(Mailchimp $mailChimp, array $lists)
	{
		$this->mailChimp = $mailChimp;
		$this->lists = $lists;
	}

	public function get($id)
	{
		return $this->lists[$id];
	}

	/**
	 * [getMembers description]
	 * @param  int $listID the unique id for the list
	 * @return array         data
	 */
	public function getMembers($listID)
	{
		return $this->mailChimp->get('lists/' .$listID .'/members');
	}

	public function getMember($listID, $email)
	{
		return $this->mailChimp->get('lists/' .$listID .'/members/' .md5(strtolower($email)));
	}

	public function memberExists($listID, $email)
	{
		try {
			$this->getMember($listID, $email);
			return true;
		}catch (\Exception $e) {
			return false;
		}
	}

	public function addMember($listID, $data)
	{
		return $this->mailChimp->post('lists/' .$listID .'/members', $data);
	}

	public function updateMember($listID, $email, $data)
	{
		return $this->mailChimp->patch('lists/' .$listID .'/members/' .md5(strtolower($email)), $data);
	}

	public function removeMember($listID, $email)
	{
		return $this->mailChimp->delete('lists/' .$listID .'/members/' .md5(strtolower($email)));
	}

	public function subscribe($listID, $email)
	{
		if ($this->memberExists($listID, $email)) {
			$this->updateMember($listID, $email, [
				'status' => 'subscribed',
			]);
		}else {
			$this->addMember($listID, [
				'email_address' => $email,
				'status' => 'subscribed',
			]);
		}

		return true;
	}

	public function unsubscribe($listID, $email)
	{
		if ($this->memberExists($listID, $email)) {
			$this->updateMember($listID, $email, [
				'status' => 'unsubscribed',
			]);
		}

		return true;
	}

}