<?php

namespace Gentlefox\Mailchimp;

use Curl\Curl;

class Mailchimp {

	const MAILCHIMP_ROOT = 'api.mailchimp.com/3.0';

	/**
	 * @var Curl
	 */
	private $curl;

	/**
	 * us1 or us2 etc
	 * @var string
	 */
	private $dataCenter = 'us1';

	/**
	 * username
	 * @var string
	 */
	private $username;

	/**
	 * API Key
	 * @var string
	 */
	private $key = '';

	/**
	 * the protocol to use (http or https)
	 * @var string
	 */
	private $protocol = 'http';

	/**
	 * @var string
	 */
	private $rootURL;

	/**
	 * @param string the datacenter (US1, US2 etc)
	 */
	public function __construct($dataCenter, $username, $apiKey)
	{
		$this->curl = new Curl();
		$this->dataCenter = $dataCenter;
		$this->username = $username;
		$this->key = $apiKey;
		$this->curl->setBasicAuthentication($this->username, $this->key);
		$this->rootURL = $this->protocol .'://' .$this->dataCenter .'.' .self::MAILCHIMP_ROOT;
	}

	public function getDataCenter()
	{
		return $this->dataCenter;
	}

	public function setDataCenter($dc)
	{
		$this->dataCenter = $dc;
		$this->rootURL = $this->protocol .'://' .$this->dataCenter .'.' .self::MAILCHIMP_ROOT;
	}

	public function getUsername()
	{
		return $this->username;
	}

	public function setUsername($username)
	{
		$this->username = $username;
		$this->curl->setBasicAuthentication($this->username, $this->key);
	}

	public function getKey()
	{
		return $this->key;
	}

	public function setKey($key)
	{
		$this->key = $key;
		$this->curl->setBasicAuthentication($this->username, $this->key);
	}

	public function get($path, $data = null)
	{
		if ($data) {
			if ( ! is_string($data)) $data = json_encode($data);
			$this->curl->get($this->rootURL .'/' .$path, $data);
		}else {
			$this->curl->get($this->rootURL .'/' .$path);
		}
		
		if ($this->curl->error) {
			throw new \Exception('Curl Error: ' .$this->curl->error_code);
		}

		return json_decode($this->curl->response, true);
	}

	public function post($path, $data = null)
	{
		if ($data) {
			if ( ! is_string($data)) $data = json_encode($data);
			$this->curl->post($this->rootURL .'/' .$path, $data);
		}else {
			$this->curl->post($this->rootURL .'/' .$path);
		}
		
		if ($this->curl->error) {
			throw new \Exception('Curl Error: ' .$this->curl->error_code);
		}

		return json_decode($this->curl->response, true);
	}

	public function put($path, $data = null)
	{
		if ($data) {
			if ( ! is_string($data)) $data = json_encode($data);
			$this->curl->put($this->rootURL .'/' .$path, $data);
		}else {
			$this->curl->put($this->rootURL .'/' .$path);
		}
		
		if ($this->curl->error) {
			throw new \Exception('Curl Error: ' .$this->curl->error_code);
		}

		return json_decode($this->curl->response, true);
	}

	public function patch($path, $data = null)
	{
		if ($data) {
			if ( ! is_string($data)) $data = json_encode($data);
			$this->curl->patch($this->rootURL .'/' .$path, $data);
		}else {
			$this->curl->patch($this->rootURL .'/' .$path);
		}
		
		if ($this->curl->error) {
			throw new \Exception('Curl Error: ' .$this->curl->error_code);
		}

		return json_decode($this->curl->response, true);
	}

	public function delete($path, $data = null)
	{
		if ($data) {
			if ( ! is_string($data)) $data = json_encode($data);
			$this->curl->delete($this->rootURL .'/' .$path, $data);
		}else {
			$this->curl->delete($this->rootURL .'/' .$path);
		}
		
		if ($this->curl->error) {
			throw new \Exception('Error ' .$this->curl->error_code .':' .$this->curl->response);
		}

		return json_decode($this->curl->response, true);
	}

	public function lists()
	{
		return new ListManager($this, $this->get('lists'));
	}

}